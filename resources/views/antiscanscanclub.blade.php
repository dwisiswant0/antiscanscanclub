<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="UTF-8" name="charset"/>
	<meta content="no" http-equiv="imagetoolbar"/>
	<meta content="English" name="language"/>
	<meta content="ID" name="geo.country"/>
	<meta content="Indonesia" name="geo.placename"/>
	<meta content="7" name="revisit-after"/>
	<meta content="1" name="revisit"/>
	<meta content="general" name="rating"/>
	<meta content="global" name="distribution"/>
	<meta content="global" name="target"/>
	<meta content="index, follow" name="robots"/>
	<meta content="index, follow, snipet" name="googlebot"/>
	<meta content="all" name="webcrawlers"/>
	<meta content="all" name="spiders"/>
	<meta content="all" name="webcrawlers"/>
	<meta content="all" name="audience"/>
	<meta content="follow, all" name="Googlebot-Image"/>
	<meta content="follow, all" name="Scooter"/>
	<meta content="follow, all" name="msnbot"/>
	<meta content="follow, all" name="alexabot"/>
	<meta content="follow, all" name="Slurp"/>
	<meta content="follow, all" name="ZyBorg"/>
	<meta content="noobSecurity" name="creator"/>
	<meta content="summary" name="twitter:card"/>
	<meta content="@noobsec_org" name="twitter:creator"/>
	<meta content="https://www.antiscanscan.club" property="og:url"/>
	<meta content="Prevent your Site from Hackers! A Laravel Package to Block Automated Scanners from Scanning your Site." property="og:description"/>
	<meta content="Anti Scan Scan Club | noobSecurity" property="og:title"/>
	<meta content="website" property="og:type"/>
	<meta content="https://www.antiscanscan.club/img/47261277-ae3b5480-d4f5-11e8-8055-aaf090f198c4.png" property="og:image"/>
	<title>Anti Scan Scan Club | noobSecurity</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css'>
	<style type="text/css">
		html, body, .page {
		  width: 100%;
		  height: 100%;
		  margin: 0;
		  padding: 0;
		  transition: all .6s cubic-bezier(.5, .2, .2, 1.1);
		  -webkit-transition: all .6s cubic-bezier(.5, .2, .2, 1.1);
		  -moz-transition: all .6s cubic-bezier(.5, .2, .2, 1.1);
		  -o-transition: all .6s cubic-bezier(.5, .2, .2, 1.1);  
		  color: #fff;
		  overflow-x: hidden; 
		}

		* {
		  font-family: 'open sans', 'lato', 'helvetica', sans-serif;
		}

		.page {
		  position: absolute;
		}

		#intro {
		  left: 0;
		}

		#usage textarea {
			margin: 0px;
			resize: none;
			border: 0;
			padding: 10px;
			border-radius: 10px;
			overflow: hidden;
		}

		#installation, #test, #usage, #p5 {
		  left: 200%;
		}

		#intro { background: darkslateblue; }
		#installation { background: tomato; }
		#test { background: maroon; }
		#usage { background: deeppink; }
		#p5 { background: rebeccapurple; }

		#test > section {
		    height: 500px;
		}
		#usage > section {
		    height: 300px;
		}

		#installation:target #installation,
		#test:target #test,
		#usage:target #usage,
		#t5:target #p5 {
		  transform: translateX(-190%);
		  -webkit-transform: translateX(-190%);
		  -moz-transform: translateX(-190%);
		  -o-transform: translateX(-190%);
		  transition-delay: .4s !important;
		}

		#installation:target #intro, 
		#test:target #intro,
		#usage:target #intro,
		#t5:target #intro{
		  background: black;
		}

		#installation:target #intro .icon, 
		#test:target #intro .icon,
		#usage:target #intro .icon,
		#t5:target #intro .icon {
		  -webkit-filter: blur(3px);
		}

		.icon {
		  color: #fff;
		  font-size: 32px;
		  display: block;
		}

		ul .icon:hover {
		  opacity: 0.5;
		}

		.page .icon .title, .subtitle {
		  line-height: 2;
		}

		#installation:target ul .icon,
		#test:target ul .icon,
		#usage:target ul .icon,
		#t5:target ul .icon{
		  transform: scale(.6);
		  -webkit-transform: scale(.6);
		  -moz-transform: scale(.6);
		  -o-transform: scale(.6);
		  transition-delay: .25s;
		}

		#installation:target #dos,
		#test:target #tres,
		#usage:target #cuatro,
		#usage:target #cinco {
		  transform: scale(1.2) !important;
		  -webkit-transform: scale(1.2) !important;
		  -moz-transform: scale(1.2) !important;
		  -o-transform: scale(1.2) !important;
		}

		ul {
		  position: fixed;
		  z-index: 1;
		  top: 0;
		  bottom: 0;
		  left: 0;
		  margin: auto;
		  height: 280px;
		  width: 10%;
		  padding: 0;
		  text-align: center;
		 }

		#menu .icon {
		  margin: 30px 0;
		  transition: all .5s ease-out !important;
		  -webkit-transition: all .5s ease-out;
		  -moz-transition: all .5s ease-out;
		  -o-transition: all .5s ease-out;
		}

		a {
		  text-decoration: none;
		}

		.title, .subtitle, .hint {
		  display: block;
		}

		.title {
		  font-size: 38px;
		}

		.subtitle {
			font-size: 20px;
		    border-bottom: 2px dashed #fff;
		    border-top: 2px dashed #fff;
		    margin: 0px auto 0;
			margin-bottom: 10px;
    		margin-top: 30px;
		}

		.hint {
		  font-size: 13px;
		}

		#usage .hint {
		  display: inherit !important;
		}

		.hint a {
		  color: yellow;
		  transition: all 250ms ease-out;
		  -webkit-transition: all 250ms ease-out;
		  -moz-transition: all 250ms ease-out;
		  -o-transition: all 250ms ease-out;
		}

		.hint a:hover {
		  color: #FFF;
		}

		.line-trough {
		  text-decoration: line-through;
		}

		.page .icon {
		  position: absolute;
		  top: 0;
		  bottom: 0;
		  right: 10%;
		  left: 0;
		  height: 170px;
		  margin: auto;
		  text-align: center;
		  font-size: 80px;
		  line-height: 1.3;
		  transform: translateX(360%);
		  -webkit-transform: translateX(360%);
		  -moz-transform: translateX(360%);
		  -o-transform: translateX(360%);
		  transition: all .5s cubic-bezier(.25, 1, .5, 1.25);
		  -webkit-transition: all .5s cubic-bezier(.25, 1, .5, 1.25);
		  -moz-transition: all .5s cubic-bezier(.25, 1, .5, 1.25);
		  -o-transition: all .5s cubic-bezier(.25, 1, .5, 1.25);
		}

		.page#intro .icon {
		  height: 220px;
		}

		.page#intro .icon {
		  transform: translateX(10%) !important;
		}

		#installation:target .page#installation .icon,
		#test:target .page#test .icon,
		#usage:target .page#usage .icon,
		#t5:target .page#p5 .icon {
		  transform: translateX(0) !important;
		  -webkit-transform: translateX(0) !important;
		  -moz-transform: translateX(0) !important;
		  -o-transform: translateX(0) !important;
		  transition-delay: 1s;
		}

		#centered {
		  font-size: 14px;
		  max-width: 300px;
		  margin: 0px auto 0;
		}

		#centered input, #centered textarea {
		  color: white;
		  font-family: 'Open Sans', sans-serif;
		  font-weight: 300;
		  width: 100%;
		  margin: 3px 0;
		  text-indent: 5px;
		  padding: 10px 0;
		  border-radius: 3px;
		  background: firebrick;
		  border: 0;
		}

		#centered input::placeholder, #centered textarea::placeholder {
			color: silver;
		}

		input[type="submit"] {
		  background-color: #4d90fe;
		  color: white;
		  border: 0;
		}

		input[type="submit"]:hover {
		  background-color: #4d9aff;
		  color: white;
		}
	</style>
</head>
<body>
<div class="ct" id="intro">
  <div class="ct" id="installation">
    <div class="ct" id="usage">
	  <div class="ct" id="test">
		  <ul id="menu">
			<a href="#intro"><li class="icon fa fa-shield" id="uno"></li></a>
			<a href="#installation"><li class="icon fa fa-cogs" id="dos"></li></a>
			<a href="#usage"><li class="icon fa fa-book" id="cuatro"></li></a>
			<a href="#test"><li class="icon fa fa-code" id="tres"></li></a>
		  </ul>
		  <div class="page" id="intro">
			 <section class="icon fa fa-shield"><span class="title">AntiScanScanClub</span>
			 	<p class="hint">
			 		Prevent your Site from Hackers!<br>
			 		A Laravel Package to Block Automated Scanners from Scanning your Site.<br>
			 		made with <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAL2SURBVGhD7ZnLi9NQFMajGx8I4kZEfKK4EhGbezuVQuhNKwqiLsQHCrrShe+FuFOXutB/QMSNKG5V1JUb/wKRwRGX4sC4kE6TtL6m9bvtGcPgMU3TtLlIfvAxbXrvOd/tObnpZaycnJwc82nusdd7rrjgKfnKd+UU/vp43/KU+IzXL3HtarNS2kjD+9Kqyc2+Etf0XGi6F0v6FFtfO69z0vDkBI5cB5P3oV8I3olSb4x4FJW4WS1swJjHseK54qfOHVR3r6XpgxEoeQjJPC54tITnufZRCvMHryKPJ40XVIoHKEw8UMLL0BwfsL88V7Z1DApn4f0VfY0bG0fai6eKlyhcNIErDw6TbF7dRVTFESQ+lko8LKJvJXo9372Z2CCDSzR64j5LICVmI+8JfdOwEw0SqnmP7C6ku1XG2B2ylt6d2J0OH1zkJpgotPk5sh2C0rzgBpsodMpzsh3iK/mBG2yk4JVsh6S6W4xcokG2Q/QWxQ82UPBKtkOwqkl2sJESk2Q7BH31hB9soOCVbIdgGz3BDjZQnrJPku2Qr9XCSpQmwa/F8QrPgKBeLq8i2wtBFW5zk4ySknfI7t94tdJqk7fT7g9NR6whuzw4yJzlJpugWGeCjmUtQhVecwEylRJvOjesxWQzGl0m3A+f2EBZSIkZfVYhe/EIVEFiES024BiFvv/eqBbLZGsw9BEuyzOCzq2PpGQnGfoBl8Ui9DlabyhkYzgaSh5GKb9xiUYhmP+BjeQUpU8HLKCCh0idS5imunu9svdR2nRBP+5AgmkucSpS8kvg2oLSjYaWU9yE8r5lDQwjJd/PumIbpRktnf2F5ejTB6yRJFLy4YzjrKDw4wMPmNNoqYA1FUP6ORO4xTMULhu8mr0d3+A7zmAfTfk1sZPCZEt779YlaKlbcZ4XqNgcKne3PTGxjKabQ9MVJX0zcsZ7Eh8T/ywYFx3HWYpFXEd/N+eN69fQTSO/9X+h/42EtnqKlnlWr+zaQpdzcnJy/iss6zdFXvBim//0MQAAAABJRU5ErkJggg==" style="width: 15px"> by <a href="https://dw1.co" target="_blank"><b>dw1</b></a> from <a href="https://noobsec.org" target="_blank"><b>noobSecurity</b></a>.<br><br>
			 		<img src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg">
			 	</p>
			 </section>
		  </div>
		  <div class="page" id="installation">
			<section class="icon fa fa-cogs"><span class="title">Installation</span>
				<p class="hint">
					Simply just type:<br>
					<code>$ composer require noobsec/antiscanscanclub-laravel</code>
					<br><br>
					<a href="https://github.com/noobsec/AntiScanScanClub-laravel/releases"><img src="https://img.shields.io/github/release/noobsec/AntiScanScanClub-laravel/all.svg"></a>
				</p>
			</section>
		  </div>
		  <div class="page" id="usage">
			<section class="icon fa fa-book">
			  <span class="title">Usage</span>
			  <p class="hint">
				Below is basic:<br>
				<textarea style="width: 355px; height: 38px;" disabled>$ASSC = new AntiScanScanClub();
$blocker = TRUE;
$ASSC->filterInput($request->all(), $blocker, $request->ip());</textarea>
			  </p>
			  <p class="hint">For more methods, check <a href="https://github.com/noobsec/AntiScanScanClub-laravel/blob/master/README.md" target="_blank">README.md</a> from our official <a href="https://github.com/noobsec/AntiScanScanClub-laravel" target="_blank">repo</a>.</p>
			</section>
		  </div>
		  <div class="page" id="test">
			<section class="icon fa fa-code">
			  <span class="title">Testing Payloads</span>
			  <p class="hint">
			  	Put your best payloads below, enjoy ya if don't return <b>403</b>. In this test, <b>blocker</b> options are set to <b>TRUE</b>.<br>So, to clean your IP from blacklists, go to <a href="/clean">/clean</a> URL and back to home to try another payloads, h3h3!<br>
			  	<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAbRSURBVGhD7VcLTFNXGK5G3RbRacx8xznnjDOZZnFQJPaBQlsktIVSHEpUQEEFF2cUH9lw+CDq2AB1PkAUcSBjQ0VHBHG8jAZF5yYiggMZgo+BKIpbnK7/zn/vue1te13GeouL8Uu+9Pzn/P///ff0PO6VvMSLAlcf/TipSh8hVWmzpGrtRcImqUr3iCG2sQ/HiI+7ym8sDXvu6OGu1geRIs9J1TroCt3U2rMkzoA52FTdjA+mB4whM1ouVFxX6KbSlpLf0TRt98BNrZO5qXTttsX8V5J/4+4Ulc6DpncupN5aKbuuhYv5z1TpOjA3lXEOXKfrB5GZbxYsQBRqm6b6+g6kcuKDiKTbi4pLsjz3Ujlx4abyn0Q27V9CoqISNbz071JZ8UBmJkVQ0AlELSorDjQazStk7T8QEhOLU30NPFt7f/LkiN5U3nG4qnVqLrkmaC6s2fg5T0wcJuxIBd/geZY+lZ83lXccJGEcJnXX6KGsohKCI5dYiYvBxTGfQOmZs2abXHCxVN5xkIvmKCfSdq8DZs6PMgtNIQ/FtbtKfuyKuHjr3CrdESrvOMj6/xmT7tqfxYgUlZ0mD/MpJKemQ/jSleYiusrVG7ZA9pHvYVtaBjQ0tTC5NyZ+Rce1F6m84yDJ2jBpbn4hI8LnzPnRVkV1hdGr1trlS/06mx1X6VqpvOPgXh0KSsrtBOdEL7Mr7N9y5bpNdvn25xzixjupvOMgyUyYVOgfWP/FNquiukKcbdt8uJzouInKOw58W3yWYF7BD1ZFIaf4+IN+boTZVuiCrMY5nj73o12+2C2JzBhqUnnHQfZAPSZdtX6zneCt31rByzDLqrC4hGRmeXB2fNIO8NR/aOWjDQmH1vb7dvkCwxYx4+QY/YXKOw7yAOcxqW7OAjtBJJ4keEegD27qX1tuQ+ymL83F7kzPhMzcPLONxBjbPPVNzeY8hJVU3nGQY7SIE750pdZOGJl/soS5TesbbzA2f2+kZeUwfSkZByFgXiTzy4/leLy4zBxD3odOUHnHQf7OVC5x6gH7fSBEfBguRmi2hZicso/3ANrdVN5xuKn8ornEkcvXCIrz2dh8k/HjYjIPHRX0syVejlyMVKNbTOUdh1Sjn8olxs2Y/k0u3GlrFyyiqqYO/GaHWQoh9JgRANv3Zgj6c8Q7RhUYYo4R9RvZQ6vtR5IydwFHbl3z2XTzNviTNc734xM3s20MMiPnsLUv+ahRKIwuVF4ckE11jRPAk2JdwlaI3ZwItQ3XzYXwj04h4stbQckpsz+eVsdOFFteHyiJVh2VFQ9kU+3gi3A0hC6Ey1evwYbE7XZjQsRL7WT5GeblLTjyI6bP9o2WnHrJVFY8SL31PnwRPvHmFep/FvHri39T29LNW6+isuKB+axUax8KCYpJ1EAtKisuyI18OGp2IHRWHAZouwJ/bAmFjvwUaCnOguNJsRAZHipYFJ/RYXOhkPjeIjEP8nfD4z0xYLpTBfdKssBoMOIDfEflxAc5TkMqtOMBblwAqC4CUPYCiHIHeNjI0JQUAR2FaXApMwmSVkaB1t8IelLUtphFUJ2VCA8L9wAkLjD7M7HefQGaKgHqymCfzyT8DphJ5cTHBKOxz3W5yxNYPQPgYwWATMLydDZATTGAJ3mg+FmWAjM/Y8nZOIY+6IsxXHxcIMBCVyhSDv8dNaicc1AtH1BtFuaIM7lsGtvGGW2tBuhoAAgazRLb2Idj6IO+GMPPQZivHFlEZZyHOkW/UFthO+bEAxTstNjYxj6+jw2fynpAtvwtVyrjPIBE0uOm7LX7VgXgjAa9abHD3gNY8L7Fxjb2cTb6xhksNmGF8o16KuF8/KQYsMQsPq0PQG0JQEKoVUH/SPQlpxgEv83YT2Q94aDXmOk0ffegUe7SwhSzdRG7QcsOWAoMnwgw+x2LjW3s42z0xZhLBQBer0KpcugFmrb7UCUb6PGnrKcJKg+xxbTXWTZp7maAXUstBWMb+7CNPuiLMZdPwF3fIU9zNWNH0rTdi/OKQWngP4yc4aVsQcu9ATT9yYlTw94T3ANgG/twDH3Ql8SYdIMhWzk6hqbrfpAN3bNO3r8WjKPYsz17A7u+sUAkLh0kZ+MY+uC/ph8CJcqh5TTV88OtiZK+jfK+raB2YfdDFVnXXMG4dJCcjWObQpgbvFIxqH4tmQCa5vniuuL1AVfl/ZuZ5WIYDrBnBcCFPICKb1liO3U5QABZbsTnlHxwTY5xgnNv3K4CJkt6lymG5j3Gjc2tfRt2yHqbjnqOSqch/08c8xwxqUQ5rLhB7vKoU97LhEVfk/frLFCMKNjvM34cdXuJFwQSyd9JDcf3gGDv8gAAAABJRU5ErkJggg==">
			  </p>
			  <div class="hint" style="margin-bottom: 50px">
				<div id="centered">
		  			<span class="subtitle">GET Request</span>
				    <form method="GET">
				      <textarea name="payload" rows="8" placeholder="'-1 and (select 1)=(Select 0xA*1000)+UnIoN+SeLeCT+1,2,version(),4,5,database(),user(),8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26"></textarea>
				      <input type="submit" value="Send" id="send"><br>
				    </form>
				</div>
				<div id="centered">
		  			<span class="subtitle">POST Request</span>
				    <form method="POST">
				      <textarea name="payload" rows="8" placeholder="<object/onerror=write`1`//"></textarea>
				      <input type="hidden" name="_token" value="{{ csrf_token() }}">
				      <input type="submit" value="Send" id="send"><br>
				    </form>
				</div>
			  </div>
			</section>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
</body>
</html>