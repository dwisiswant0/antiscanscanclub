<!DOCTYPE html>
<html>
<head>
	<title>Anti Scan Scan Club | noobSecurity</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css'>
	<style type="text/css">
		html, body {
		  margin: 5% !important;
		  background: darkslateblue;
		  width: 100%;
		  height: 100%;
		  margin: 0;
		  padding: 0;
		  transition: all .6s cubic-bezier(.5, .2, .2, 1.1);
		  -webkit-transition: all .6s cubic-bezier(.5, .2, .2, 1.1);
		  -moz-transition: all .6s cubic-bezier(.5, .2, .2, 1.1);
		  -o-transition: all .6s cubic-bezier(.5, .2, .2, 1.1);  
		  color: #fff;
		  overflow-x: hidden; 
		  max-width: 800px;
		}

		code {
			background: black;
		}

		a {
		  text-decoration: none;
		  color: yellow;
		  transition: all 250ms ease-out;
		  -webkit-transition: all 250ms ease-out;
		  -moz-transition: all 250ms ease-out;
		  -o-transition: all 250ms ease-out;
		}

		a:hover {
		  color: #FFF;
		}

		* {
		  font-family: 'open sans', 'lato', 'helvetica', sans-serif;
		}
	</style>
</head>
<body>
<h1>Good research!</h1>
<code>{{ $e->getMessage() }}</code><br>
/[REDACTED]/{{ preg_replace("/^(\/home\/.*?\/)/i", "", $e->getFile()) }}:{{ $e->getLine() }}<br>
<hr>
<br>Please copy these errors &amp; open <a href="https://github.com/noobsec/AntiScanScanClub-laravel/issues/new" target="_blank">new issue</a>.<br>Your name will be listed on <b>contributors</b>.<br><br>Regards!<br><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATWSURBVGhD7VlLb1tFGLV4SDw2dFFVFNGsGjtJaUPqlNhxlSI2LFChNPwLECDRpCuQaBASEoIUiW4olEgoqAlIbEj6oi2tqtohDSWoiYoaN61w/cgDCTUPFt/H+cbjcInn2uNrW24lH+nI1zNzvu/M3Hnca/vqqKMOOzDvfJjSkUbKhF+mdPhtynT2UbrzCD6HORM+R8mOUYoHR+na9ot0pXmKfmlapJh/VXE8sEjjzdOqTtqkQqeUBlodA7EkZniv5GDuekinLQ+UCbUj8HsIfBGJ/uFMJztJqfAC3Qyep4mWBMcCXArp120Jmg1eQuy/8+IiF8ovIPe7+AxqO/aAyI8gl/IDhxdRHsUIfss32s5hlJdN5kohYqzQTNsFDMYg7sTPyJ3Oy6s6E2nU9ooDJqe08AZ4iDKRFygR2ajqYv79FAukTGbKIeIm+XLTKyrHXMdTGKRuDNgAvMwpL/AkdVaA6etZUXgWwo8oFXmR4zuegPHDpuSVJHL0U6J1IzzsxR05DKZ0B6a1veKghVADzJ8VYY74TpzoYL61izkeZL7+LPO1HcyTzzBf3cY80cI83gw2ZTkG5ozJda5c2khb0UxuR4xW5j/amGcQU2LfQQ5H3mzuzp9ofvfT2p49KL27DcY/pETHAqfDeYGrRuSSnDJ9ORVu1Xa8gaKBT9dG8gpGT0Z8Wo9afCfzbDvz7eeY/8TIyejdCTEn0dmUo8NyLWVSJ22krWhEKzEklsT8HbElh84nubUNb0CAfWvma0DkJ/Gg7ZQGnlCLtuK7TalUuxO8aFv2oKj/M1PAWlB2Jm3LDjQWeBKisg+pShFelsSTtlccmHcfmALVkuhEn7ZXGMy+BzB9Zk1Bakl4uinetE130NjWXaYA9wIp1tiubbqDo4GDJvF6Lpxu5qMHQvzOq3sUv8B1ehQnrKGtk151Qrrc1KNtugPzf8gkdnLxdAufGR7gleUlHhn8mt946XnFg6918V9n3M141eUo3rRNd2CuTZrETh7tCSkTguW7d9eMCL/swSlr0Ai96nJEB37TNt2Bg2PeJHZSbv3I4IAy8eM3x/5n5MD+LqNG6FWXI7zNaZvuwB1YNYmdFCPO5E4W64BJI7TqALxpm+6w6YAsPJMJoSxQk0boVZejXQcsppDsGr1YeOtN9Hbv4bkTeM43aIRedTnaTqGii1gou4YsPLn1Pd1dagRttkOvOqHdIrbYRmtFq23U9iCrBe0OMhzXJvG9QEzv4r8R3fcPcwLMtfv3cVqgX2iWTIEK8eRbj/PI64/yyvmteXVSJnWn0GZ9XTGW/EIjgKjfFKwQo4c2KJPxY5vz6ma+2qzqpM36umIUL9qWPby81CeHtiiTJ998THVCRl0o1ydQJnWp4Qaj1o2eX+oFWAv7QDIFduPU55uUUROnj2wyatwoucWDtuMNCPDfD1uWTH63haN9G9SaEMq1lJnaFiJ2nk+0De/g474H0YkfTAmqSZUTubWN8sBnGx7BXPzelKgaVLmQU6evDNSdkJ++S1wTpVBiw/zHFRt5E+RPCLUzGAyUQxVT/8FRdegtth8s/y8mxFAbhdetshxkT2z/+16enUSjtKWesNWAfgAMYiR7wSGM6lWYm0fZqqJcowzGj2fb+IPWD2Z11FFHAfh8/wJcOIgJT4k+pQAAAABJRU5ErkJggg==">
</body>
</html>