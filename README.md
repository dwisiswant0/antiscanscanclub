# AntiScanScan.club Project

## Setup

```bash
$ git clone https://gitlab.com/dwisiswant0/antiscanscanclub.git
$ cd antiscanscanclub/
$ composer install
$ npm install
$ cp .env.example .env
$ echo 'ASSC_LIST="blacklists.json"' >> .env
$ php artisan cache:clear && php artisan config:clear && php artisan config:cache && php artisan optimize:clear && php artisan view:clear
$ php artisan key:generate
$ npm run dev
$ php artisan serve
```

## Requirement

```
- PHP (v7.2)
- nodejs (v9 and above)
- npm (v6 and above)
```
