<?php

namespace App\Http\Middleware;

use Closure;
use noobsec\AntiScanScanClub\AntiScanScanClub;

class AntiScanScanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ASSC = new AntiScanScanClub();
        $blocker = TRUE;

        if ($request->path() === "clean") {
            $ASSC->removeFromBlacklists($request->ip());
        } else {
            $ASSC->checkIp($request->ip());

            if ($request->isMethod('GET') && $request->getQueryString() === NULL) {
                /**
                 * Prevention of access to credentials and/ important files/path
                 * (e.g: wp-admin.php, .git/, backups.tar.gz, www.sql)
                 */

                $ASSC->filterFile($request->getPathInfo(), $blocker, $request->ip());
            } else {
                $ASSC->filterInput($request->all(), $blocker, $request->ip());
            }
        }

        return $next($request);
    }
}
